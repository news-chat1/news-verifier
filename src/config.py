import os

mongo_host = os.environ.get('MONGO_HOST', 'localhost')
mongo_port = int(os.environ.get('MONGO_PORT', '27017'))
mongo_user = os.environ.get('MONGO_USER')
mongo_pass = os.environ.get('MONGO_PASS')
mongo_db = os.environ.get('MONGO_DB', 'articles')
mongo_collection = os.environ.get('MONGO_COLLECTION', 'articles_hashes')

subscription_topic = os.environ.get('SUBSCRIPTION_TOPIC')
push_topic = os.environ.get('PUSH_QUEUE')
rabbitmq_host = os.environ.get('RABBITMQ_HOST', 'localhost')
rabbitmq_port = int(os.environ.get('RABBITMQ_PORT', '5672'))
rabbitmq_user = os.environ.get('RABBITMQ_USER')
rabbitmq_pass = os.environ.get('RABBITMQ_PASS')
prefetch_count = int(os.environ.get('PREFETCH_COUNT', '1'))
workers = int(os.environ.get('WORKERS', '12'))

# MONGO_HOST=localhost;MONGO_PORT=27017;MONGO_USER=myuser;MONGO_PASS=mypassword; MONGO_DB=articles;MONGO_COLLECTION=articles_hashes;SUBSCRIPTION_TOPIC=article_discover;PUSH_QUEUE=parse_articles;RABBITMQ_HOST=localhost;RABBITMQ_PORT=5672;RABBITMQ_USER=myuser;RABBITMQ_PASS=mypassword;PREFETCH_COUNT=1;WORKERS=12
