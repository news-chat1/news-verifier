from hashlib import md5


def hash_string(s: str):
    s = s.strip()
    encoded_str = s.encode('utf-8')
    return md5(encoded_str).hexdigest()
