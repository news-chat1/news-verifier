from core.db import MongoDBDatabase
from . import helpers
from .config import mongo_host, mongo_port, mongo_user, mongo_pass, mongo_db, mongo_collection

mongo = MongoDBDatabase(host=mongo_host, port=mongo_port, user=mongo_user, password=mongo_pass,
                        database=mongo_db, collection_name=mongo_collection)

mongo.connect()


def check_if_article_exists(url, title):
    """Check if article exists in MongoDB

    Args:
        article_hash (str): Article hash

    Returns:
        bool: True if article exists, False otherwise
    """

    full_string = url + title
    hash = helpers.hash_string(full_string)
    exists = mongo.check_exists_by_id(hash)
    return exists


def insert_article(url, title):
    """Insert article into MongoDB

    Args:
        article_hash (str): Article hash
    """
    full_string = url + title
    hash = helpers.hash_string(full_string)
    mongo.insert_one({'_id': hash, 'url': url, 'title': title})