from newspaper import Article

from core.queues.Consumer import BaseConsumer
from .url_checker import check_if_article_exists, insert_article


class VerifierConsumer(BaseConsumer):
    def parse_message(self, body) -> bool:
        try:
            article = Article(url=body)
            article.download()
            if article.title == '':
                return True

            article.parse()
            exists = check_if_article_exists(url=body, title=article.title)
            if not exists:
                insert_article(url=body, title=article.title)
                queue = self.queue
                queue.send(body, topic=self.push_topic)
                return True
            else:
                return True

        except Exception as e:
            print(f'Error: {e}')
            return False
