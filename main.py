from src.config import *
from src.consumer import VerifierConsumer

if __name__ == '__main__':

    verifier_consumer = VerifierConsumer(
        user=rabbitmq_user, password=rabbitmq_pass, subscribe_topic=subscription_topic, push_topic=push_topic,
        host=rabbitmq_host, port=rabbitmq_port, prefetch_count=prefetch_count, workers=workers)
    verifier_consumer.run()
