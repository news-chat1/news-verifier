import json
from typing import Any, Callable, Optional
from core.logging.log import setup_logging

import pika

from ._interfaces import BaseQueueInterface
logger = setup_logging(__name__)

class RabbitMQQueue(BaseQueueInterface):
    def __init__(self, user: str, password: str, topic: str, host: str = 'localhost', port: int = 5672,
                 prefetch_count: int = 1):
        """A RabbitMQ Service.

        Args:
            user (str): The username of the RabbitMQ server.
            password (str): The password of the RabbitMQ server.
            topic (str): The subscription id of the RabbitMQ server.
            host (str, optional): The host of the RabbitMQ server. Defaults to 'localhost'.
            port (int, optional): The port of the RabbitMQ server. Defaults to 5672.
        """

        self.topic: str = topic
        self._credentials: pika.PlainCredentials = pika.PlainCredentials(user, password)
        self._connection_params: pika.ConnectionParameters = pika.ConnectionParameters(host=host, port=port,
                                                                                       credentials=self._credentials,
                                                                                       heartbeat=6000)
        self.connection: pika.BlockingConnection = None
        self.channel: pika.adapters.blocking_connection.BlockingChannel = None

        self.prefetch_count: int = prefetch_count
        self.connect()

    def connect(self):
        """Connect to the RabbitMQ server."""
        logger.info(f'Connecting to RabbitMQ server at {self._connection_params.host}:{self._connection_params.port}')
        self.connection = pika.BlockingConnection(self._connection_params)
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=self.topic, durable=True, exclusive=False, auto_delete=False)
        self.channel.basic_qos(prefetch_count=self.prefetch_count)
        logger.info(f'Connected to RabbitMQ server at {self._connection_params.host}:{self._connection_params.port}')

    def disconnect(self):
        """Disconnect from the RabbitMQ server."""
        logger.info(f'Disconnecting from RabbitMQ server at {self._connection_params.host}:{self._connection_params.port}')
        if self.connection:
            self.connection.close()
            self.channel = None
        logger.info(f'Disconnected from RabbitMQ server at {self._connection_params.host}:{self._connection_params.port}')

    def send(self, message: Any, topic: Optional[str] = None):
        """Publish a message to a topic.

        Args:
            message (Any): The message to publish.
            topic (Optional[str], optional): The topic to publish to. Overwrites default topic.
        """
        logger.debug(f'Publishing RabbitMQ message: ', extra={'message': message, 'topic': topic})
        if isinstance(message, dict):
            message = json.dumps(message)
        if topic is None:
            topic = self.topic

        if self.channel.is_closed:
            self.connect()

        self.channel.queue_declare(queue=topic, durable=True, exclusive=False, auto_delete=False)
        self.channel.basic_publish(exchange='',
                                   routing_key=topic,
                                   body=message)
        logger.debug(f'Published RabbitMQ message: ', extra={'message': message, 'topic': topic})

    def subscribe(self, callback: Callable[[Any], None]):
        """Subscribe to a topic with a callback function.

        Args:
            callback (Callable[[Any], None]): The callback function to call when a message is received.
        """
        if self.channel.is_closed:
            self.connect()
        self.channel.queue_declare(queue=self.topic, durable=True, exclusive=False, auto_delete=False)
        self.channel.basic_consume(queue=self.topic, on_message_callback=callback, auto_ack=False)
        self.channel.start_consuming()

    def ack(self, message):
        """Acknowledge a message.
        Args:
            message: The message to acknowledge.

        """
        logger.debug(f'Acknowledging RabbitMQ message: ', extra={'message': message})
        if self.channel.is_closed:
            self.connect()
        delivery_tag = message.delivery_tag
        self.channel.basic_ack(delivery_tag=delivery_tag)
        logger.debug(f'Acknowledged RabbitMQ message: ', extra={'message': message})

    def get_message(self, timeout: Optional[int] = None) -> Any:
        """Poll a topic for a message with an optional timeout.

        Args:
            timeout (Optional[int], optional): The timeout in seconds. Defaults to None.
        """
        logger.debug(f'Getting RabbitMQ message: ', extra={'timeout': timeout})
        if self.channel.is_closed:
            self.connect()
        method_frame, header_frame, body = self.channel.basic_get()
        if method_frame:
            self.channel.basic_ack(method_frame.delivery_tag)
            logger.debug(f'Got RabbitMQ message: ', extra={'message': body})
            return body

        logger.debug(f'No RabbitMQ message: ')
        return None
