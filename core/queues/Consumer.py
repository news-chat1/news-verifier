from concurrent.futures import ThreadPoolExecutor

from core.logging import setup_logging
from . import RabbitMQQueue

logger = setup_logging(__name__)


class BaseConsumer:
    def __init__(self, user: str, password: str, subscribe_topic: str, push_topic: str = None, host: str = 'localhost',
                 port: int = 5672, prefetch_count: int = 1, workers: int = 1):
        self.subscription_topic: str = subscribe_topic
        self.push_topic: str = push_topic
        self.rabbit_mq_user: str = user
        self.rabbit_mq_password: str = password
        self.rabbit_mq_host: str = host
        self.rabbit_mq_port: int = port
        self.prefetch_count: int = prefetch_count
        self.workers: int = workers

        self.queue = RabbitMQQueue(user=self.rabbit_mq_user,
                                   password=self.rabbit_mq_password,
                                   topic=self.subscription_topic,
                                   host=self.rabbit_mq_host,
                                   port=self.rabbit_mq_port,
                                   prefetch_count=self.prefetch_count)

    def parse_message(self, body) -> bool:
        print(f'Got message: {body}')
        return False

    def run(self):
        logger.info(f'Starting consumer with {self.workers} workers')

        def process_message(channel, method, properties, body):
            logger.debug(f'Processing message: {body}')
            message = body.decode('utf-8')
            channel.basic_ack(delivery_tag=method.delivery_tag)
            parsed = self.parse_message(message)
            if not parsed:
                channel.basic_publish(exchange='',
                                      routing_key=self.subscription_topic,
                                      body=message)
            logger.info(f'Finished processing message: {body}', extra={'parsed': parsed})

        with ThreadPoolExecutor(max_workers=self.workers) as executor:
            for i in range(self.workers):
                logger.info(f'Starting worker {i}')
                queue = RabbitMQQueue(user=self.rabbit_mq_user,
                                      password=self.rabbit_mq_password,
                                      topic=self.subscription_topic,
                                      host=self.rabbit_mq_host,
                                      port=self.rabbit_mq_port,
                                      prefetch_count=self.prefetch_count)
                queue.connect()
                executor.submit(queue.subscribe, callback=process_message)

# class RabbitMQConsumer(BaseConsumerInterface):
#
#     def __init__(self, queue: BaseQueueInterface, func: Callable[[tuple, BaseQueueInterface], None]):
#         """Initializes the consumer
#
#         Args:
#             queue (BaseQueueInterface): The queue to consume from
#         """
#         self.queue: BaseQueueInterface = queue
#         self.process_function = func
#
#     def process_message(self, channel, method, properties, body):
#         """Process a message.
#         Args:
#             channel: The channel the message was received on
#             method: The method the message was received on
#             properties: The properties of the message
#             body: The body of the message
#         """
#         message = (channel, method, properties, body)
#         self.process_function(message, self.queue)
#
#     def consume(self):
#         self.queue.connect()
#
#         def consume_message(channel, method, properties, body):
#             json_message = json.loads(body)
#
#         while True:
#             try:
#                 self.queue.subscribe(callback=self.process_message)
#             except Exception as e:
#                 logger.error(f'Error while consuming from queue', extra={'error': e})
#
#     def stop_consuming(self):
#         """Stops the consumer"""
#         self.queue.disconnect()
#
#
# class ThreadedRabbitMQConsumer:
#     def __init__(self, queue_class, queue_params: tuple, process_function: Callable[[tuple, BaseQueueInterface], None],
#                  workers: int = 1):
#         self.queue_params = queue_params
#         self.queue_class = queue_class
#         self.process_function = process_function
#         self.workers: int = workers
#
#     def run(self):
#         with ThreadPoolExecutor(max_workers=self.workers) as executor:
#             for _ in range(self.workers):
#                 queue_instance = self.queue_class(*self.queue_params)
#                 consumer = RabbitMQConsumer(queue_instance, self.process_function)
#                 executor.submit(consumer.consume)
